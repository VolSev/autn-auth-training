import { makeUsersRepository } from "../users/repository";
import { makeAuthController } from "./controller";
import { makeAuthRouter } from "./router";
import token from "./verifyToken";

const repository = makeUsersRepository();
const authController = makeAuthController({ repository, token });
const router = makeAuthRouter({ authController });

export default router;
