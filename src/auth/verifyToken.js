import jwt from "jsonwebtoken";

function signToken({ data }) {
  return jwt.sign(data, "secret");
}

export default Object.freeze({
  signToken
});
