function makeAuthRouter({ authController }) {
  return router => {
    router.post("/auth", authController.login);
  };
}

export { makeAuthRouter };
