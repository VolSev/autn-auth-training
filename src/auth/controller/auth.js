function makeAuthController({ repository, token }) {
  async function login(req, res, next) {
    let credentials = req.body.credentials;
    let user;
    try {
      user = (await repository.findOne({ email: credentials.email }))[0];
      if (!user) {
        throw new Error("There is no user with such email.");
      }
      if (user.password !== credentials.password) {
        throw new Error("Wrong email or passwrod.");
      }
    } catch (e) {
      res.status(400).send(e);
    }
    let genToken = token.signToken({
      data: { firstName: user.firstName, lastName: user.lastName }
    });
    res
      .status(200)
      .header("Authorization", genToken)
      .send({ token: genToken });
  }

  return Object.freeze({
    login
  });
}

export { makeAuthController };
