import express from "express";
import swaggerUi from "swagger-ui-express";
import YAML from "yamljs";
const swaggerDocument = YAML.load("./swagger.yaml");
const app = express();

const apiRouter = express.Router();
require("./users").default(apiRouter);
require("./auth").default(apiRouter);

app.use(express.json());
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.get("/", (req, res) => {
  res.send("Azazaz");
});

app.use("/api", apiRouter);

app.listen(3000, () => {
  console.log("App listening on port 3000!");
});
