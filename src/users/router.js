export function makeUsersRouter({ usersController }) {
  return function usersRouter(router) {
    router.get("/users", usersController.getUsers);
    router.post("/users", usersController.postUser);
  };
}
