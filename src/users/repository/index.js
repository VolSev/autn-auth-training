import { User, knex } from "./model/index";

function makeUsersRepository() {
  async function getUsers() {
    // return User.query().where({});
    return knex.raw(`SELECT
    table_schema || '.' || table_name as show_tables
FROM
    information_schema.tables
WHERE
    table_type = 'BASE TABLE'
AND
    table_schema NOT IN ('pg_catalog', 'information_schema');`);
  }

  async function postUser({ user }) {
    return User.query().insert(user);
  }

  async function findOne({ email }) {
    return User.query().where({ email });
  }

  return Object.freeze({
    getUsers,
    postUser,
    findOne
  });
}

export { makeUsersRepository };
