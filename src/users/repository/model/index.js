import Knex from "knex";
import { Model } from "objection";
import config from "../../../../knexfile";
import User from "./users";

const knex = Knex(config);
Model.knex(knex);

export { User, knex };
