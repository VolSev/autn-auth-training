import { makeUsersRepository } from "./repository";
import { makeUsersController } from "./controller";
import { makeUsersRouter } from "./router";

const repository = makeUsersRepository();
const usersController = makeUsersController({ repository });
const router = makeUsersRouter({ usersController });

export default router;
