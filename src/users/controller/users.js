function makeUsersController({ repository }) {
  async function getUsers(req, res, next) {
    let users;
    try {
      users = await repository.getUsers();
    } catch (e) {
      res.status(400).send(e);
    }
    res.status(200).send(users);
  }

  async function postUser(req, res, next) {
    var user = req.body.user;
    let result;
    try {
      result = await repository.postUser({ user });
    } catch (e) {
      res.status(400).send(e);
    }
    res.status(200).send(result);
  }

  return Object.freeze({
    getUsers,
    postUser
  });
}

export { makeUsersController };
