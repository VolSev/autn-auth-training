exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex("users")
    .del()
    .then(function() {
      // Inserts seed entries
      return knex("users").insert([
        {
          id: 1,
          firstName: "Steave1",
          lastName: "Jobs",
          email: "jobst1@localStorage.apple",
          password: "111111"
        },
        {
          id: 2,
          firstName: "Steave2",
          lastName: "Jobs",
          email: "jobst2@localStorage.apple",
          password: "111111"
        },
        {
          id: 3,
          firstName: "Steave3",
          lastName: "Jobs",
          email: "jobst3@localStorage.apple",
          password: "111111"
        }
      ]);
    });
};
