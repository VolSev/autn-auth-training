exports.up = function(knex) {
  return knex.schema.alterTable("users", table => {
    table.string("email");
    table.string("password");
  });
};

exports.down = function(knex) {
  return knex.schema.alterTable("users", table => {
    table.dropColumn("email");
    table.dropColumn("password");
  });
};
